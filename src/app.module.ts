import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import databaseConfig from "./database/database.config";
import {UserEntity} from "./database/entities/user.entity";
import {UserDetailsEntity} from "./database/entities/user-details.entity";

@Module({
    imports: [
        TypeOrmModule.forRoot(databaseConfig),
        TypeOrmModule.forFeature([UserEntity, UserDetailsEntity])
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
