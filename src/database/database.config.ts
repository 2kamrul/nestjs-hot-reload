import {MysqlConnectionOptions} from "typeorm/driver/mysql/MysqlConnectionOptions";

import * as path from 'path'
const sourcePath = path.join(__dirname, "./");

const databaseConfig: MysqlConnectionOptions = {
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "root",
    database: "nestjs_hot_reload",
    entities: [sourcePath + "/entities/**/*.entity{.ts,.js}"],
    synchronize: true,
    logging: true,
};

export default databaseConfig;
