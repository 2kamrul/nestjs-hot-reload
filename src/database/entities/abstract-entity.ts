import {
    BaseEntity,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from "typeorm";

export abstract class AbstractEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    ID: number;

    @CreateDateColumn({select: false})
    CREATED_AT: Date;

    @UpdateDateColumn({select: false})
    UPDATED_AT: Date;

    @DeleteDateColumn({select: false})
    DELETED_AT: Date;

    @Column({select: false})
    CREATED_BY: number;

    @Column({select: false})
    UPDATED_BY: number;

    @Column({select: false})
    DELETED_BY: number;
}
