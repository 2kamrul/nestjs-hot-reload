import { Column, Entity } from "typeorm";
import {AbstractEntity} from "./abstract-entity";

@Entity("user_details")
export class UserDetailsEntity extends AbstractEntity {
    @Column()
    user_id: number;

    @Column()
    address: string;

    @Column()
    country: string;
}
