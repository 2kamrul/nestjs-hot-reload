import {Column, Entity} from "typeorm";
import {AbstractEntity} from "./abstract-entity";

@Entity("user")
export class UserEntity extends AbstractEntity {
    @Column()
    name: string;

    @Column()
    name2: string;


    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    mobile: string;
}
